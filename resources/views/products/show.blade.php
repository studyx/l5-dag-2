@extends('app')

@section('content')
    <h2>{{ $product->name }}</h2>
    <hr />
    <div class="body">
        {{ $product->description }}
    </div>
    <div class="price">
        {{ $product->price }}
    </div>
    <br>
    <i>Aangemaakt op {{ $product->created_at }}</i>
    <br>

    @unless($product->tags->isEmpty())

        <h5>Tags</h5>
        <ul>
            @foreach($product->tags as $tag)
                <li>{{ $tag->name }}</li>
            @endforeach
        </ul>
    @endunless

    <a class="btn btn-primary" href="{{ action('ProductController@index') }}">Terug naar overzicht.</a>
@stop