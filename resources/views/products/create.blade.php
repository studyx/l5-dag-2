@extends('app')

@section('content')
    <h2>Product aanmaken</h2>
    <hr />
    {!! Form::open(['url' => 'products']) !!}
        @include('products.form', ['submitButtonText' => 'Product aanmaken'])
    {!! Form::close() !!}
@stop