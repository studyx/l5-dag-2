@extends('app')

@section('content')
    @forelse ($products as $product)
        <h2>{{ $product->name }}</h2>
        <p>{{ $product->description }}</p>
        <em>{{ $product->price }}</em>
        <a class="btn btn-warning" href="{{ action('ProductController@edit', [$product->id]) }}">Wijzigen</a>
        {!! Form::open([
            'method' => 'DELETE',
            'route' => ['products.destroy', $product->id]
        ]) !!}
        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
    @empty
        <p>No products</p>
    @endforelse
    <a class="btn btn-primary" href="{{ action('ProductController@create') }}">Nieuw product aanmaken.</a>
@stop