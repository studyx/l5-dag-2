@extends('app')

@section('content')
    <h2>Product {{ $product->name }} wijzigen</h2>
    <hr />
    {!! Form::model($product, ['method' => 'PUT', 'url' => 'products/'. $product->id]) !!}
        @include('products.form', ['submitButtonText' => 'Product editeren'])
    {!! Form::close() !!}
@stop